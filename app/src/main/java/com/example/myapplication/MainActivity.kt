package com.example.myapplication

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.myapplication.adapter.NotesAdapter
import com.example.myapplication.adapter.NotesTouchHelper
import kotlinx.android.synthetic.main.activity_main.*
import model.Note

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val list = mutableListOf<Note>()

        for (i in 1..100) {
            list.add(Note(i, "Title " + i, "Description " + i))
        }

        val adapter = NotesAdapter(this, list)

        recyclerNotes.layoutManager = LinearLayoutManager(this)

        val notesTouchHelper = ItemTouchHelper(NotesTouchHelper(adapter))
        notesTouchHelper.attachToRecyclerView(recyclerNotes)

        recyclerNotes.adapter = adapter
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_list_style, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menu_list -> recyclerNotes.layoutManager = LinearLayoutManager(this)
            R.id.menu_grid -> recyclerNotes.layoutManager = GridLayoutManager(this, 2)
        }

        return super.onOptionsItemSelected(item)
    }
}
