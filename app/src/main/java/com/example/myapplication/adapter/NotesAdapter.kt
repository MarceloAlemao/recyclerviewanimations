package com.example.myapplication.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.R
import kotlinx.android.synthetic.main.item_notes.view.*
import model.Note
import java.util.*

class NotesAdapter(private val context: Context, private val notesList: MutableList<Note>) :
    RecyclerView.Adapter<NotesAdapter.NotesViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NotesViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_notes, parent, false)
        return NotesViewHolder(view)
    }

    override fun getItemCount(): Int {
        return notesList.size
    }

    override fun onBindViewHolder(holder: NotesViewHolder, position: Int) {
        holder.bind(notesList[position])
    }

    class NotesViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val txtTitle = itemView.txtTitle
        private val txtDescription = itemView.txtDescription

        fun bind(note: Note) {
            txtTitle.text = note.title
            txtDescription.text = note.description
        }
    }

    fun remove(position: Int) {
        notesList.removeAt(position)
        notifyItemRemoved(position)
    }

    fun move(initialPosition: Int, targetPostition: Int) {
        Collections.swap(notesList, initialPosition, targetPostition)

        notifyItemMoved(initialPosition, targetPostition)
    }
}