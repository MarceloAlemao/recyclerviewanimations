package com.example.myapplication.adapter

import android.view.View
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView

class NotesTouchHelper(private val adapter: NotesAdapter) : ItemTouchHelper.Callback() {
    override fun getMovementFlags(
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder
    ): Int {
        val movementFlags = ItemTouchHelper.LEFT + ItemTouchHelper.RIGHT
        val dragFlags =
            ItemTouchHelper.DOWN + ItemTouchHelper.UP + ItemTouchHelper.LEFT + ItemTouchHelper.RIGHT

        return makeMovementFlags(dragFlags, movementFlags)
    }

    override fun onMove(
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        target: RecyclerView.ViewHolder
    ): Boolean {
        val initialPosition = viewHolder.adapterPosition
        val targetPosition = target.adapterPosition

        adapter.move(initialPosition, targetPosition)

        return true
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
        val position = viewHolder.adapterPosition

        adapter.remove(position)
    }
}